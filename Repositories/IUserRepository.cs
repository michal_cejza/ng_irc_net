﻿using IRC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IRC.Repositories
{
    public interface IUserRepository
    {
        /// <summary>
        /// Get all users
        /// </summary>
        /// <returns></returns>
        Task<IList<ApplicationUser>> GetAllUsers();
    }
}
