﻿using IRC.Data;
using IRC.Models;
using IRC.Repositories;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IRC.Implementations
{
    public class UserRepository: ApplicationUser, IUserRepository
    {
        readonly ApplicationDbContext _context;
        public UserRepository(ApplicationDbContext context) 
        {
            _context = context;
        }
        public async Task<IList<ApplicationUser>> GetAllUsers() 
        {
            return await _context.Users.Select(s => s).ToListAsync();
        }
    }
}
