﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IRC.Models
{
    public class Message
    {
        public string ClientUniqueId { get; set; }
        public string Type { get; set; }
        public string MessageText { get; set; }
        public DateTime MessageDate { get; set; }
    }
}
