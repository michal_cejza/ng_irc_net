﻿using IRC.Models;
using Microsoft.AspNetCore.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IRC.Interfaces
{
    public interface IChat
    {
        Task NewMessage(Message msg);
    }
}
