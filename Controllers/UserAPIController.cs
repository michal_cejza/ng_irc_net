﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.DependencyInjection;
using IRC.Repositories;
using Microsoft.AspNetCore.Identity;
using IRC.Models;

namespace IRC.Controllers
{
    [Authorize]
    [ApiController]
    [Route("api/[controller]")]
    public class UserAPIController: ControllerBase
    {

        private readonly ILogger<UserAPIController> _logger;
        private readonly IUserRepository _userRepo;
        private readonly UserManager<ApplicationUser> _userManager;

        public UserAPIController(ILogger<UserAPIController> logger, IServiceProvider service)
        {
            _logger = logger;
            _userRepo = service.GetService<IUserRepository>();
            _userManager = service.GetService<UserManager<ApplicationUser>>();
        }

        [HttpGet]
        [Route("GetAllUsers")]
        public async Task<IActionResult> GetAllUsers()
        {
            try 
            {
                return Ok(await _userRepo.GetAllUsers());
            }
            catch (Exception ex) 
            {
                return BadRequest();
            }
        }
    }
}
