export class Message {

  public clientUniqueId: string;
  public type: string;
  public messageText: string;
  public messageDate: Date;

  constructor() {

  }
}
