import { Component, OnInit } from '@angular/core';
import { AuthorizeService } from '../../api-authorization/authorize.service';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
})
export class HomeComponent implements OnInit {

  private isAuthenticated: Observable<boolean>;

  constructor(private authorizeService: AuthorizeService, private router: Router) {
  }

  ngOnInit() {
    this.isAuthenticated = this.authorizeService.isAuthenticated();
    this.isAuthenticated.subscribe((response) => {
      if (response === false) {
        this.router.navigate(['/authentication/login']);
      }
    }, (error) => {
        console.log(error);
    });
  }
}
