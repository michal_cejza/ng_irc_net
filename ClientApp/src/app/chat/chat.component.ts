import { Component, OnInit, NgZone } from '@angular/core';
import { Message } from '../models/message';
import { ChatService } from '../services/chat.service';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.css']
})
export class ChatComponent implements OnInit {

  public messageText: string;
  public message = new Message();
  public messages: Array<Message> = [];

  constructor(private chatService: ChatService, private ngZone: NgZone) {
    this.subscribeToEvent();
  }

  ngOnInit() {
  }

  sendMessage(): void {
    if (this.messageText != "") {
      this.message = new Message();
      this.message.clientUniqueId = new Date().getTime().toString();
      this.message.type = "sent";
      this.message.messageText = this.messageText;
      this.message.messageDate = new Date();
      this.messages.push(this.message);
      this.chatService.sendMessage(this.message);
      this.messageText = "";
      this.messages.push(this.message);
    }
  }

  subscribeToEvent(): void {
    this.chatService.messageReceived.subscribe((message: Message) => {
      this.ngZone.run(() => {
        if (message.clientUniqueId !== message.clientUniqueId) {
          message.type = "received";
          this.messages.push(message);
        }
      });
    });
  }

}
