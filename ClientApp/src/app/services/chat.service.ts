import { EventEmitter, Injectable, Inject } from '@angular/core';
import { HubConnection, HubConnectionBuilder } from '@aspnet/signalr';
import { Message } from '../models/message';

@Injectable({
  providedIn: 'root'
})
export class ChatService {

  public messageReceived = new EventEmitter<Message>();
  public connectionEstablished = new EventEmitter<boolean>();
  private connectionIsEstablished = false;
  private hubConnection: HubConnection;
  private baseUrl: string;

  constructor(@Inject('BASE_URL') baseUrl: string) {
    this.baseUrl = baseUrl;
    //set connection
    this.hubConnection = new HubConnectionBuilder().withUrl(baseUrl + 'Chat').build();
    //register events
    this.hubConnection.on("MessageReceived", (data: any) => {
      this.messageReceived.emit(data);
    });
    //start connection
    this.hubConnection.start().then(() => {
      this.connectionIsEstablished = true;
      console.log('Connected');
      this.connectionEstablished.emit(true);
    }).catch((error) => {
      console.log(error);
    });
  }

  sendMessage(message: Message): void {
    this.hubConnection.invoke('NewMessage', message);
  }
}
